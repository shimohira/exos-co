import { ExosCoPage } from "./app.po";

describe("exos-co App", () => {
  let page: ExosCoPage;

  beforeEach(() => {
    page = new ExosCoPage();
  });

  it("should display welcome message", () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual("Welcome to app!!");
  });
});
