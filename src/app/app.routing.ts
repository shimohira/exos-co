import { Routes, RouterModule } from "@angular/router";
import { LoginComponent } from "app/login/login.component";
import { HomeComponent } from "app/home/home.component";
import { AuthGuard } from "app/login/auth.guard";

const appRoutes: Routes = [
    { path: "login", component: LoginComponent },
    { path: "", component: HomeComponent, canActivate: [AuthGuard]},
    { path: "**", redirectTo: "" }
];

export const routing = RouterModule.forRoot(appRoutes);
