import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {

    // lineChart
  public lineChartData: Array<any> = [
    {data: [65, 59, 80, 81, 56, 55, 40],
label: "CO"}
  ];
  public lineChartLabels: Array<any> = ["10:05", "10:10", "10:15", "10:20", "10:25", "10:30", "10:35"];
  public lineChartType = "line";

  public randomizeType(): void {
    this.lineChartType = this.lineChartType === "line" ? "bar" : "line";
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
  constructor() { }

  ngOnInit() {
  }
}
